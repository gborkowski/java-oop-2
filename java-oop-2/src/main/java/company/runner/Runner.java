package company.runner;

import company.staff.*;

public class Runner {
    public static void main(String[] args) {
        Company companySingleton;
        companySingleton = Company.getCompany();

        CEO companyCEO = new CEO("Zbyszek", 20000);
        companySingleton.hireCEO(companyCEO);

        Employee LeszekEmployee = new Employee("Leszek", 10000);
        Employee WacekEmployee = new Employee("Wacek", 8000);

        Manager MaciekManager = new CountManager("Maciek", 8000, 2);
        MaciekManager.hireEmployee(LeszekEmployee);
        MaciekManager.hireEmployee(WacekEmployee);
        Employee GrzesiekEmployee = new Employee("Grzesiek", 5000);
        Manager KrzysiekManager = new CountManager("Krzysiek", 6000, 2);

        Employee FrodoEmployee = new Employee("Frodo", 3500);
        KrzysiekManager.hireEmployee(FrodoEmployee);


        companyCEO.hireEmployee(KrzysiekManager);
        companyCEO.hireEmployee(MaciekManager);
        companyCEO.hireEmployee(GrzesiekEmployee);


        System.out.println(companySingleton.toString());
    }
}
