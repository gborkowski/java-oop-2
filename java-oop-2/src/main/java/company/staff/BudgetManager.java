package company.staff;

public class BudgetManager extends Manager {

    private int budget;

    public BudgetManager(String name, int salary, int budget) {
        super(name, salary);
        this.budget = budget;
    }

    @Override
    public boolean canHireEmployee(Employee employeeToHire) {
        int sum = getSalaryOfEmployees();
        if(employeeToHire.getSalary() + sum > budget) {
            return false;
        }
        return true;
    }
}
