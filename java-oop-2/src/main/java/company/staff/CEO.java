package company.staff;

public class CEO extends Manager {

    public CEO(String name, int salary) {
        super(name, salary);
    }

    @Override
    public boolean canHireEmployee(Employee e) {
        return true;
    }


    @Override
    public String toString() {
        String toReturn = this.getName() + "-CEO\n";
        for(Employee employee: this.employeeList) {
            toReturn += "\t" + employee.toString();
        }
        return toReturn;
    }
}
