package company.staff;

public class Company {
    // initialized during class loading
    private static final Company INSTANCE  = new Company();
    private CEO companyCEO;

    // to prevent creating another instance of Singleton
    private Company() {}

    public static Company getCompany() {
        return INSTANCE;
    }

    public void hireCEO(CEO companyCEO) {
        this.companyCEO = companyCEO;
    }

    public CEO getCompanyCEO() {
        return companyCEO;
    }

    @Override
    public String toString() {
       return companyCEO.toString();
    }
}
