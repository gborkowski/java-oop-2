package company.staff;

public class CountManager extends Manager {
    private int count;

    public CountManager(String name, int salary, int count) {
        super(name, salary);
        this.count = count;
    }

    @Override
    public boolean canHireEmployee(Employee employeeToHire) {
        int employeeNumber = getEmployeeNumber();
        if(employeeNumber >= count) {
            return false;
        }
        return true;
    }
}
