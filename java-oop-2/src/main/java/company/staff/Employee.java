package company.staff;

public class Employee {
    protected String name;
    protected int salary;
    protected boolean satisfied = true;
    protected static int satisfactionSalary = 10000;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
        setSatisfied();
    }

    private void setSatisfied() {
        satisfied = salary >= satisfactionSalary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
        setSatisfied();
    }

    public boolean isSatisfied() {
        return satisfied;
    }

    @Override
    public String toString() {
        return name + "-Employee \n";
    }
}
