package company.staff;

import java.util.ArrayList;
import java.util.List;

public abstract class Manager extends Employee {

    List<Employee> employeeList;

    Manager(String name, int salary) {
        super(name, salary);
        employeeList = new ArrayList<Employee>();
    }

    public boolean canHireEmployee(Employee e) {
        return false;
    }

    public void hireEmployee(Employee e) {
        if (canHireEmployee(e)) {
            employeeList.add(e);
        }
    }

    int getSalaryOfEmployees() {
        int sum = 0;
        for (Employee employee : employeeList) {
            sum += employee.getSalary();
        }
        return sum;
    }

    int getEmployeeNumber() {
        return employeeList.size();
    }

    @Override
    public String toString() {
        String toReturn = name + "-Manager\n";
        for (Employee employee : employeeList) {
            toReturn+= "\t" + employee.toString();
        }
        return toReturn;
    }
}
