package company.staff;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

public class TestEmployees {

    private Map<String, Employee> employeeMap;

    @Before
    public void setUp() {
        Employee unsatisfied = new Employee("Jacek", 5000);
        Employee satisfied = new Employee("Wacek", 12500);
        employeeMap = new HashMap<String, Employee>();
        employeeMap.put("Jacek", unsatisfied);
        employeeMap.put("Wacek", satisfied);
    }

    @Test
    public void testUnsatisfiedEmployee() {
        assertEquals("Employee with salary 5000 should be unsatisfied!", false, employeeMap.get("Jacek").isSatisfied());
    }

    @Test
    public void testSatisfiedEmployee() {
        assertEquals("Employe with salary 12500 should be satisfied!", true, employeeMap.get("Wacek").isSatisfied());
    }
}
