package company.staff;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class TestManagers {

    private Manager countManager;
    private Manager budgetManager;
    private Employee[] expectedCountManagerEmployees = new Employee[2];
    private Employee[] expectedBudgetManagerEmployees = new Employee[1];

    @Before
    public void setUp() {
        countManager = new CountManager("Maciek", 20000, 2);
        budgetManager = new BudgetManager("Krzysiek", 12500, 6000);

        Employee wacekEmployee = new Employee("Wacek", 10000);
        Employee leszekEmployee = new Employee("Leszek", 7000);
        Employee zdzisiekEmployee = new Employee("Zdzisiek", 11500);
        Employee frodoEmployee = new Employee("Frodo", 5000);


        /* only Wacek and Leszek should be hired beacuse
            countManager can hire only 2 employees
         */
        countManager.hireEmployee(wacekEmployee);
        countManager.hireEmployee(leszekEmployee);
        countManager.hireEmployee(zdzisiekEmployee);

        expectedCountManagerEmployees[0] = wacekEmployee;
        expectedCountManagerEmployees[1] = leszekEmployee;

        /* only Frodo should be hired beacuse Zygfryd's salary
            exceeds Managers' limitt
         */
        budgetManager.hireEmployee(new Employee("Zygfryd", 10000));
        budgetManager.hireEmployee(frodoEmployee);

        expectedBudgetManagerEmployees[0] = frodoEmployee;
    }

    @Test
    public void testCountManagerHireEmployes() {
        Assert.assertArrayEquals(expectedCountManagerEmployees, countManager.employeeList.toArray());
    }

    @Test
    public void testBudgetManagerHireEmployees() {
        Assert.assertArrayEquals(expectedBudgetManagerEmployees, budgetManager.employeeList.toArray());
    }
}
